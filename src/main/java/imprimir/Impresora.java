package imprimir;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

public class Impresora {

	static int pos = 0;
	static String archivo = "";
	static String puerto = "";

	public static void main(String args[]) {
		Properties properties = new Properties();
		try {
			File a = crearConf();
			properties.load(new FileInputStream(a));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"ERROR AL CARGAR EL ARCHIVO CONF");
		}
		archivo = properties.getProperty("ruta.archivo");
		puerto = properties.getProperty("puerto.impresora");
		boolean bn = true;
		if (archivo == null || archivo.isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"EL PARAMETRO RUTA.ARCHIVO NO ESTA CONFIGURADO");
			bn = false;
		}
		if (puerto == null || puerto.isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"EL PARAMETRO PUERTO.IMPRESORA NO ESTA CONFIGURADO");
			bn = false;
		}

		if (bn) {
			Impresora p = new Impresora();
			do {
				List<String> linea = leerArchivo();
				if (linea != null && !linea.isEmpty()) {
					for (String s : linea) {
						System.out.println(s);
						if (s.contains("SUBTOTAL 12%.................$")) {
							System.out.println("***1");
							p.abrirCajon();
						} else if (s.compareToIgnoreCase("fin") == 0) {
							System.out.println("***2");
							p.correr(10);
							p.cortar();
							vaciarArchivo();
						}  else {
							p.setFormato(1);
							p.setNegro();
							p.escribir(s);
						}
					}
				}
			} while (true);
		}
	}

	public static File crearConf() {
		File directorio = new File("conf.properties");
		if (!directorio.exists()) {
			FileWriter fw = null;
			PrintWriter pw = null;
			try {
				fw = new FileWriter(directorio);
				pw = new PrintWriter(fw);
				pw.print("ruta.archivo=\npuerto.impresora=");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (fw != null)
						fw.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
		return directorio;
	}

	public static void vaciarArchivo() {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(archivo);
			pw = new PrintWriter(fichero);
			pw.print("");
			pos = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	@SuppressWarnings("resource")
	public static List<String> leerArchivo() {
		FileReader fr = null;
		List<String> linea = new ArrayList<String>();
		try {
			fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);
			int l = 0;
			String aux;
			while ((aux = br.readLine()) != null) {
				if (l == pos) {
					linea.add(aux);
					pos++;
				}
				l++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null)
					fr.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return linea;
	}

	private FileWriter fw;
	private PrintWriter pw;

	public Impresora() {
		try {
			fw = new FileWriter(puerto);
			pw = new PrintWriter(fw);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void escribir(String texto) {
		pw.println(texto);
	}

	public void cortar() {
		char[] ESC_CUT_PAPER = new char[] { 0x1B, 'm' };
		pw.write(ESC_CUT_PAPER);
	}

	public void abrirCajon() {
		pw.write(27);
		pw.write(112);
		pw.write(0);
		pw.write(25);
		pw.write(250);
	}

	public void avanza_pagina() {
		pw.write(0x0C);
	}

	public void setRojo() {
		char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r', 1 };
		pw.write(ESC_CUT_PAPER);
	}

	public void setNegro() {
		char[] ESC_CUT_PAPER = new char[] { 0x1B, 'r', 0 };
		pw.write(ESC_CUT_PAPER);
	}

	public void setTipoCaracterLatino() {
		char[] ESC_CUT_PAPER = new char[] { 0x1B, 'R', 18 };
		pw.write(ESC_CUT_PAPER);
	}

	public void setFormato(int formato) {
		char[] ESC_CUT_PAPER = new char[] { 0x1B, '!', (char) formato };
		pw.write(ESC_CUT_PAPER);
	}

	public void correr(int fin) {
		for (int i = 1; i <= fin; i++)
			this.salto();
	}

	public void salto() {
		escribir("");
	}

	public void dibujaLinea(PrintWriter ps) {
		escribir("----------------------------------------");
	}

	public void cerrarDispositivo() {
		pw.close();
	}

}